describe('parts', function(){
    it('defines a parts object', function(){
        expect(parts).toBeDefined()
    })
})

describe('build', function(){
    var constructorA, constructorB, result
    beforeEach(function(){
        constructorA = jasmine.createSpy()
        constructorA.and.returnValue('Test Instance A')
        constructorB = jasmine.createSpy()
        constructorB.and.returnValue('Test Instance B')
        result = build({
            a: constructorA,
            b: constructorB
        })
    })
    it('calls each constructor provided in the argument', function(){
        expect(constructorA.calls.count()).toEqual(1)
        expect(constructorB.calls.count()).toEqual(1)
    })
    it('passes the instance container to each constructor', function(){
        expect(constructorA.calls.argsFor(0)[0]).toBe(result)
        expect(constructorB.calls.argsFor(0)[0]).toBe(result)
    })
    it('copies the returned instances to the instance container', function(){
        expect(result.a).toEqual('Test Instance A')
        expect(result.b).toEqual('Test Instance B')
    })
})