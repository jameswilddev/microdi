# MicroDI
A tiny, one line dependency injection container with no external dependencies written in JavaScript.  MicroDI defines two things; "parts", an object to add constructors to, and "build", which creates instances of a given set of parts.

## Application code example
    parts.window = function() { return window }

    parts.localizations = function() {
        return {
            helloWorldMessage: 'Hello World'
        }
    }

    parts.dialog = function(instances) {
        return {
            show: function() {
                instances.window.alert(instances.localizations.helloWorldMessage)
            }
        }
    }

    build(parts).dialog.show()

This creates three parts; a reference to the window object, a set of localization strings and a service which uses that set.  All three parts add themselves to the parts object.  build internally creates a new instances object, and calls each function in parts to get a new instance, passing the instances object as the argument.  The return value of each constructor is stored under the same name in the instances object.  The completed instances object is then returned.

On executing dialog.show, it retrieves the title from the localization instance and calls alert from the window instance.

Note that because there is no ordering of constructors, the order in which your parts are created is non-deterministic and you should not attempt to use them within constructors themselves.

On the other hand, this limitation means that circular dependencies are valid and allowed.

## Unit test example
    var fakes
    beforeEach(function(){
        fakes = {
            localizations: {
                helloWorldMessage: 'Test Hello World'
            },
            window: {
                alert: jasmine.createSpy()
            }
        }
        parts.dialog(fakes).show()
    })
    it('shows the hello world message', function(){
        expect(fakes.window.alert).toHaveBeenCalledWith('Test Hello World')
    })

Because we can access the constructor and pass a list of the instances we want them to use, testing dialog is easy.